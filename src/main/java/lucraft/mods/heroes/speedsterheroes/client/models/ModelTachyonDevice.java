package lucraft.mods.heroes.speedsterheroes.client.models;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelTachyonDevice extends ModelBase {

    public ModelRenderer Shape1;
    public ModelRenderer Shape2;
    public ModelRenderer Shape3;
    public ModelRenderer Shape4;
    public ModelRenderer Shape5;
    public ModelRenderer Shape7;
    public ModelRenderer Shape8;
    public ModelRenderer Shape9;
    public ModelRenderer Shape10;
    public ModelRenderer Shape11;
    public ModelRenderer Shape12;
    public ModelRenderer Shape13;
    public ModelRenderer Shape14;
    public ModelRenderer Shape15;
    public ModelRenderer Shape16;
    public ModelRenderer Shape17;
    public ModelRenderer Shape18;
    public ModelRenderer Shape19;
    public ModelRenderer Shape20;
    public ModelRenderer Shape21;
    public ModelRenderer Shape22;
    public ModelRenderer Shape23;
    public ModelRenderer Shape24;
    public ModelRenderer Shape25;
    public ModelRenderer Shape26;
    public ModelRenderer Shape27;
    public ModelRenderer Shape28;
    public ModelRenderer Shape29;
    public ModelRenderer Shape30;
    public ModelRenderer Shape31;
    public ModelRenderer Shape32;
    public ModelRenderer Shape33;
    public ModelRenderer Shape34;
    public ModelRenderer Shape35;
    public ModelRenderer Shape36;
    public ModelRenderer Shape37;
    public ModelRenderer Shape38;
    public ModelRenderer Shape39;
    public ModelRenderer Shape40;
    public ModelRenderer Shape41;
    public ModelRenderer Shape42;
    public ModelRenderer Shape43;
    public ModelRenderer Shape44;
    public ModelRenderer Shape45;
    public ModelRenderer Shape46;
    public ModelRenderer Shape47;
    public ModelRenderer Shape48;
    public ModelRenderer Shape49;
    public ModelRenderer Shape50;
    public ModelRenderer Shape51;
    public ModelRenderer Shape52;
    public ModelRenderer Shape53;
    public ModelRenderer Shape54;
    public ModelRenderer Shape55;
    public ModelRenderer Shape56;
    public ModelRenderer Shape57;
    public ModelRenderer Shape58;
    public ModelRenderer Shape59;
    public ModelRenderer Shape60;
    public ModelRenderer Shape61;
    public ModelRenderer Shape62;
    public ModelRenderer Shape63;
    public ModelRenderer Shape64;
    public ModelRenderer Shape65;
    public ModelRenderer Shape66;
    public ModelRenderer Shape67;
    public ModelRenderer Shape68;
    public ModelRenderer Shape69;
    public ModelRenderer Shape70;
    public ModelRenderer Shape71;
    public ModelRenderer Shape72;
    public ModelRenderer Shape73;
    public ModelRenderer Shape74;
    public ModelRenderer Shape75;
    public ModelRenderer Shape76;
    public ModelRenderer Shape77;
    public ModelRenderer Shape78;
    public ModelRenderer Shape79;
    public ModelRenderer Shape80;
    public ModelRenderer Shape81;
    public ModelRenderer Shape82;
    public ModelRenderer Shape83;

    public ModelTachyonDevice() {
        this.textureWidth = 128;
        this.textureHeight = 128;
        this.Shape13 = new ModelRenderer(this, 13, 3);
        this.Shape13.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape13.addBox(-1.5F, 10.0F, -1.5F, 3, 2, 1, 0.0F);
        this.Shape5 = new ModelRenderer(this, 50, 0);
        this.Shape5.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape5.addBox(-2.5F, -13.0F, -0.5F, 5, 3, 2, 0.0F);
        this.setRotateAngle(Shape5, 0.0F, -0.0F, -0.4101523756980897F);
        this.Shape51 = new ModelRenderer(this, 20, 15);
        this.Shape51.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape51.addBox(-2.5F, -18.0F, -1.5F, 1, 6, 2, 0.0F);
        this.setRotateAngle(Shape51, -0.1047197580337524F, -0.0F, 2.460914134979248F);
        this.Shape26 = new ModelRenderer(this, 45, 8);
        this.Shape26.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape26.addBox(-2.5F, -12.0F, -0.5F, 5, 2, 2, 0.0F);
        this.setRotateAngle(Shape26, 0.0F, -0.0F, -1.230457067489624F);
        this.Shape83 = new ModelRenderer(this, 29, 27);
        this.Shape83.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape83.addBox(-1.5F, -18.0F, -1.5F, 3, 1, 1, 0.0F);
        this.setRotateAngle(Shape83, -0.1047197580337524F, -0.0F, 0.8203047513961791F);
        this.Shape4 = new ModelRenderer(this, 36, 0);
        this.Shape4.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape4.addBox(-2.5F, -13.0F, -0.5F, 5, 3, 2, 0.0F);
        this.setRotateAngle(Shape4, 0.0F, -0.0F, -2.8710665702819824F);
        this.Shape20 = new ModelRenderer(this, 47, 5);
        this.Shape20.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape20.addBox(-1.5F, 9.0F, -0.5F, 3, 1, 2, 0.0F);
        this.setRotateAngle(Shape20, 0.0F, -0.0F, -1.5707963705062866F);
        this.Shape77 = new ModelRenderer(this, 42, 24);
        this.Shape77.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape77.addBox(0.5F, -21.0F, -1.5F, 1, 3, 2, 0.0F);
        this.setRotateAngle(Shape77, -0.1047197580337524F, -0.0F, 0.8203047513961791F);
        this.Shape36 = new ModelRenderer(this, 116, 11);
        this.Shape36.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape36.addBox(-2.5F, -12.5F, -2.0F, 2, 3, 3, 0.0F);
        this.setRotateAngle(Shape36, 0.0F, -0.0F, -0.4101523756980897F);
        this.Shape42 = new ModelRenderer(this, 0, 13);
        this.Shape42.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape42.addBox(-2.5F, -12.0F, -0.5F, 5, 2, 2, 0.0F);
        this.setRotateAngle(Shape42, 0.0F, -0.0F, 0.8203047513961791F);
        this.Shape63 = new ModelRenderer(this, 9, 20);
        this.Shape63.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape63.addBox(-1.5F, -14.0F, -0.5F, 3, 2, 3, 0.0F);
        this.setRotateAngle(Shape63, 0.0F, -0.0F, -0.8203047513961791F);
        this.Shape14 = new ModelRenderer(this, 21, 4);
        this.Shape14.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape14.addBox(-2.5F, -14.0F, 0.5F, 5, 4, 1, 0.0F);
        this.setRotateAngle(Shape14, 0.0F, -0.0F, 0.4101523756980897F);
        this.Shape55 = new ModelRenderer(this, 45, 16);
        this.Shape55.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape55.addBox(-2.5F, -18.0F, -1.5F, 1, 6, 2, 0.0F);
        this.setRotateAngle(Shape55, -0.1047197580337524F, -0.0F, -2.460914134979248F);
        this.Shape10 = new ModelRenderer(this, 96, 0);
        this.Shape10.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape10.addBox(-2.5F, -12.0F, -0.5F, 5, 2, 2, 0.0F);
        this.setRotateAngle(Shape10, 0.0F, -0.0F, 1.6406095027923584F);
        this.Shape66 = new ModelRenderer(this, 51, 21);
        this.Shape66.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape66.addBox(1.5F, -18.0F, -1.5F, 1, 6, 2, 0.0F);
        this.setRotateAngle(Shape66, -0.1047197580337524F, -0.0F, -0.8203047513961791F);
        this.Shape69 = new ModelRenderer(this, 63, 21);
        this.Shape69.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape69.addBox(-1.5F, -21.0F, -1.5F, 1, 3, 2, 0.0F);
        this.setRotateAngle(Shape69, -0.1047197580337524F, -0.0F, -0.8203047513961791F);
        this.Shape30 = new ModelRenderer(this, 0, 9);
        this.Shape30.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape30.addBox(-2.5F, -11.9F, -1.5F, 5, 2, 2, 0.0F);
        this.setRotateAngle(Shape30, 0.0F, -0.0F, -2.4609142453120048F);
        this.Shape32 = new ModelRenderer(this, 29, 9);
        this.Shape32.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape32.addBox(-2.5F, -11.9F, -1.5F, 5, 2, 2, 0.0F);
        this.setRotateAngle(Shape32, 0.0F, -0.0F, 0.8203047484373349F);
        this.Shape40 = new ModelRenderer(this, 70, 0);
        this.Shape40.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape40.addBox(8.0F, -0.5F, 0.5F, 1, 1, 1, 0.0F);
        this.setRotateAngle(Shape40, 0.0F, -0.0F, -1.5707963705062866F);
        this.Shape56 = new ModelRenderer(this, 26, 17);
        this.Shape56.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape56.addBox(-0.5F, -24.0F, -0.5F, 1, 10, 1, 0.0F);
        this.setRotateAngle(Shape56, -0.1047197580337524F, -0.0F, -2.460914134979248F);
        this.Shape44 = new ModelRenderer(this, 74, 14);
        this.Shape44.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape44.addBox(-2.5F, -12.0F, -0.5F, 5, 2, 2, 0.0F);
        this.setRotateAngle(Shape44, 0.0F, -0.0F, 2.460914134979248F);
        this.Shape73 = new ModelRenderer(this, 87, 22);
        this.Shape73.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape73.addBox(-2.5F, -18.0F, -1.5F, 1, 6, 2, 0.0F);
        this.setRotateAngle(Shape73, -0.1047197580337524F, -0.0F, 0.8203047513961791F);
        this.Shape21 = new ModelRenderer(this, 84, 6);
        this.Shape21.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape21.addBox(-1.5F, -12.0F, -1.5F, 3, 2, 1, 0.0F);
        this.setRotateAngle(Shape21, 0.0F, -0.0F, -1.5707963705062866F);
        this.Shape31 = new ModelRenderer(this, 19, 9);
        this.Shape31.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape31.addBox(0.5F, -12.5F, -2.0F, 2, 3, 3, 0.0F);
        this.setRotateAngle(Shape31, 0.0F, -0.0F, 0.4101523756980897F);
        this.Shape62 = new ModelRenderer(this, 73, 18);
        this.Shape62.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape62.addBox(-2.0F, -26.0F, -1.5F, 4, 2, 4, 0.0F);
        this.setRotateAngle(Shape62, -0.1047197580337524F, -0.0F, -2.460914134979248F);
        this.Shape37 = new ModelRenderer(this, 116, 8);
        this.Shape37.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape37.addBox(-1.5F, -10.0F, -0.5F, 3, 1, 2, 0.0F);
        this.setRotateAngle(Shape37, 0.0F, -0.0F, -1.5707963705062866F);
        this.Shape27 = new ModelRenderer(this, 89, 6);
        this.Shape27.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape27.addBox(-2.5F, -12.5F, -2.0F, 2, 3, 3, 0.0F);
        this.setRotateAngle(Shape27, 0.0F, -0.0F, 2.8710665702819824F);
        this.Shape57 = new ModelRenderer(this, 30, 17);
        this.Shape57.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape57.addBox(1.5F, -18.0F, -1.5F, 1, 6, 2, 0.0F);
        this.setRotateAngle(Shape57, -0.1047197580337524F, -0.0F, -2.460914134979248F);
        this.Shape9 = new ModelRenderer(this, 86, 0);
        this.Shape9.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape9.addBox(0.5F, -12.5F, -2.0F, 2, 3, 3, 0.0F);
        this.setRotateAngle(Shape9, 0.0F, -0.0F, 2.0507619380950928F);
        this.Shape72 = new ModelRenderer(this, 65, 24);
        this.Shape72.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape72.addBox(-2.0F, -26.0F, -1.5F, 4, 2, 4, 0.0F);
        this.setRotateAngle(Shape72, -0.1047197580337524F, -0.0F, -0.8203047513961791F);
        this.Shape74 = new ModelRenderer(this, 114, 23);
        this.Shape74.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape74.addBox(-1.5F, -14.0F, -0.5F, 3, 2, 3, 0.0F);
        this.setRotateAngle(Shape74, 0.0F, -0.0F, 0.8203047513961791F);
        this.Shape7 = new ModelRenderer(this, 64, 0);
        this.Shape7.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape7.addBox(-1.0F, 10.199999809265137F, -0.5F, 2, 2, 2, 0.0F);
        this.Shape61 = new ModelRenderer(this, 120, 17);
        this.Shape61.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape61.addBox(1.5F, -24.0F, -1.5F, 1, 4, 2, 0.0F);
        this.setRotateAngle(Shape61, -0.1047197580337524F, -0.0F, -2.460914134979248F);
        this.Shape70 = new ModelRenderer(this, 0, 22);
        this.Shape70.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape70.addBox(-2.5F, -24.0F, -1.5F, 1, 4, 2, 0.0F);
        this.setRotateAngle(Shape70, -0.1047197580337524F, -0.0F, -0.8203047513961791F);
        this.Shape80 = new ModelRenderer(this, 12, 25);
        this.Shape80.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape80.addBox(1.5F, -24.0F, -1.5F, 1, 4, 2, 0.0F);
        this.setRotateAngle(Shape80, -0.1047197580337524F, -0.0F, 0.8203047513961791F);
        this.Shape24 = new ModelRenderer(this, 12, 7);
        this.Shape24.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape24.addBox(-1.5F, -10.0F, -0.5F, 3, 1, 2, 0.0F);
        this.Shape64 = new ModelRenderer(this, 96, 20);
        this.Shape64.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape64.addBox(-2.5F, -18.0F, -1.5F, 1, 6, 2, 0.0F);
        this.setRotateAngle(Shape64, -0.1047197580337524F, -0.0F, -0.8203047513961791F);
        this.Shape25 = new ModelRenderer(this, 55, 5);
        this.Shape25.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape25.addBox(-1.5F, -18.0F, -1.5F, 3, 1, 1, 0.0F);
        this.setRotateAngle(Shape25, -0.1047197580337524F, -0.0F, -2.460914134979248F);
        this.Shape23 = new ModelRenderer(this, 75, 7);
        this.Shape23.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape23.addBox(-1.5F, -12.0F, -1.5F, 3, 2, 1, 0.0F);
        this.Shape12 = new ModelRenderer(this, 0, 4);
        this.Shape12.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape12.addBox(-2.5F, -13.0F, -0.5F, 5, 3, 2, 0.0F);
        this.setRotateAngle(Shape12, 0.0F, -0.0F, 2.8710665702819824F);
        this.Shape54 = new ModelRenderer(this, 0, 17);
        this.Shape54.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape54.addBox(-1.5F, -14.0F, -0.5F, 3, 2, 3, 0.0F);
        this.setRotateAngle(Shape54, 0.0F, -0.0F, -2.460914134979248F);
        this.Shape48 = new ModelRenderer(this, 106, 14);
        this.Shape48.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape48.addBox(-0.5F, -24.0F, -0.5F, 1, 10, 1, 0.0F);
        this.setRotateAngle(Shape48, -0.1047197580337524F, -0.0F, 2.460914134979248F);
        this.Shape43 = new ModelRenderer(this, 27, 13);
        this.Shape43.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape43.addBox(-2.5F, -12.0F, -0.5F, 5, 2, 2, 0.0F);
        this.setRotateAngle(Shape43, 0.0F, -0.0F, -2.460914134979248F);
        this.Shape35 = new ModelRenderer(this, 76, 10);
        this.Shape35.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape35.addBox(-2.5F, -11.9F, -1.5F, 5, 2, 2, 0.0F);
        this.setRotateAngle(Shape35, 0.0F, -0.0F, -0.8203047484373349F);
        this.Shape59 = new ModelRenderer(this, 92, 17);
        this.Shape59.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape59.addBox(-1.5F, -21.0F, -1.5F, 1, 3, 2, 0.0F);
        this.setRotateAngle(Shape59, -0.1047197580337524F, -0.0F, -2.460914134979248F);
        this.Shape46 = new ModelRenderer(this, 14, 13);
        this.Shape46.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape46.addBox(0.5F, -21.0F, -1.5F, 1, 3, 2, 0.0F);
        this.setRotateAngle(Shape46, -0.1047197580337524F, -0.0F, 2.460914134979248F);
        this.Shape47 = new ModelRenderer(this, 100, 14);
        this.Shape47.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape47.addBox(1.5F, -24.0F, -1.5F, 1, 4, 2, 0.0F);
        this.setRotateAngle(Shape47, -0.1047197580337524F, -0.0F, 2.460914134979248F);
        this.Shape67 = new ModelRenderer(this, 14, 10);
        this.Shape67.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape67.addBox(-1.5F, -18.0F, -1.5F, 3, 1, 1, 0.0F);
        this.setRotateAngle(Shape67, -0.1047197580337524F, -0.0F, 2.460914134979248F);
        this.Shape1 = new ModelRenderer(this, 0, 0);
        this.Shape1.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape1.addBox(-2.5F, -12.0F, -0.5F, 5, 2, 2, 0.0F);
        this.setRotateAngle(Shape1, 0.0F, -0.0F, 1.230457067489624F);
        this.Shape16 = new ModelRenderer(this, 96, 4);
        this.Shape16.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape16.addBox(-2.5F, -13.0F, -0.5F, 5, 3, 1, 0.0F);
        this.setRotateAngle(Shape16, 0.0F, -0.0F, 0.4101523756980897F);
        this.Shape50 = new ModelRenderer(this, 110, 14);
        this.Shape50.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape50.addBox(-1.5F, -21.0F, -1.5F, 1, 3, 2, 0.0F);
        this.setRotateAngle(Shape50, -0.1047197580337524F, -0.0F, 2.460914134979248F);
        this.Shape78 = new ModelRenderer(this, 81, 24);
        this.Shape78.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape78.addBox(1.5F, -18.0F, -1.5F, 1, 6, 2, 0.0F);
        this.setRotateAngle(Shape78, -0.1047197580337524F, -0.0F, 0.8203047513961791F);
        this.Shape17 = new ModelRenderer(this, 108, 4);
        this.Shape17.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape17.addBox(-2.5F, -12.0F, -0.5F, 5, 2, 2, 0.0F);
        this.Shape33 = new ModelRenderer(this, 56, 9);
        this.Shape33.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape33.addBox(-2.5F, -12.5F, -2.0F, 2, 3, 3, 0.0F);
        this.setRotateAngle(Shape33, 0.0F, -0.0F, 1.230457067489624F);
        this.Shape45 = new ModelRenderer(this, 88, 12);
        this.Shape45.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape45.addBox(-1.5F, -14.0F, -0.5F, 3, 2, 3, 0.0F);
        this.setRotateAngle(Shape45, 0.0F, -0.0F, 2.460914134979248F);
        this.Shape19 = new ModelRenderer(this, 33, 5);
        this.Shape19.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape19.addBox(-2.5F, -12.0F, -0.5F, 5, 2, 2, 0.0F);
        this.setRotateAngle(Shape19, 0.0F, -0.0F, -1.6406095027923584F);
        this.Shape68 = new ModelRenderer(this, 57, 21);
        this.Shape68.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape68.addBox(0.5F, -21.0F, -1.5F, 1, 3, 2, 0.0F);
        this.setRotateAngle(Shape68, -0.1047197580337524F, -0.0F, -0.8203047513961791F);
        this.Shape34 = new ModelRenderer(this, 66, 9);
        this.Shape34.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape34.addBox(0.5F, -12.5F, -2.0F, 2, 3, 3, 0.0F);
        this.setRotateAngle(Shape34, 0.0F, -0.0F, -1.230457067489624F);
        this.Shape75 = new ModelRenderer(this, 102, 20);
        this.Shape75.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape75.addBox(-0.5F, -24.0F, -0.5F, 1, 10, 1, 0.0F);
        this.setRotateAngle(Shape75, -0.1047197580337524F, -0.0F, 0.8203047513961791F);
        this.Shape71 = new ModelRenderer(this, 36, 22);
        this.Shape71.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape71.addBox(1.5F, -24.0F, -1.5F, 1, 4, 2, 0.0F);
        this.setRotateAngle(Shape71, -0.1047197580337524F, -0.0F, -0.8203047513961791F);
        this.Shape79 = new ModelRenderer(this, 6, 25);
        this.Shape79.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape79.addBox(-2.5F, -24.0F, -1.5F, 1, 4, 2, 0.0F);
        this.setRotateAngle(Shape79, -0.1047197580337524F, -0.0F, 0.8203047513961791F);
        this.Shape38 = new ModelRenderer(this, 48, 0);
        this.Shape38.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape38.addBox(-9.0F, -0.5F, 0.5F, 1, 1, 1, 0.0F);
        this.setRotateAngle(Shape38, 0.0F, -0.0F, -1.5707963705062866F);
        this.Shape29 = new ModelRenderer(this, 109, 8);
        this.Shape29.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape29.addBox(-2.5F, -12.5F, -2.0F, 2, 3, 3, 0.0F);
        this.setRotateAngle(Shape29, 0.0F, -0.0F, -2.0507619380950928F);
        this.Shape52 = new ModelRenderer(this, 67, 15);
        this.Shape52.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape52.addBox(1.5F, -18.0F, -1.5F, 1, 6, 2, 0.0F);
        this.setRotateAngle(Shape52, -0.1047197580337524F, -0.0F, 2.460914134979248F);
        this.Shape18 = new ModelRenderer(this, 75, 4);
        this.Shape18.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape18.addBox(-1.5F, 9.0F, -0.5F, 3, 1, 2, 0.0F);
        this.Shape81 = new ModelRenderer(this, 53, 26);
        this.Shape81.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape81.addBox(-2.0F, -26.0F, -1.5F, 4, 2, 4, 0.0F);
        this.setRotateAngle(Shape81, -0.1047197580337524F, -0.0F, 0.8203047513961791F);
        this.Shape2 = new ModelRenderer(this, 14, 0);
        this.Shape2.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape2.addBox(-1.5F, 10.0F, -1.5F, 3, 2, 1, 0.0F);
        this.setRotateAngle(Shape2, 0.0F, -0.0F, -1.5707963705062866F);
        this.Shape65 = new ModelRenderer(this, 110, 19);
        this.Shape65.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape65.addBox(-0.5F, -24.0F, -0.5F, 1, 10, 1, 0.0F);
        this.setRotateAngle(Shape65, -0.1047197580337524F, -0.0F, -0.8203047513961791F);
        this.Shape82 = new ModelRenderer(this, 9, 18);
        this.Shape82.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape82.addBox(-1.5F, -18.0F, -1.5F, 3, 1, 1, 0.0F);
        this.setRotateAngle(Shape82, -0.1047197580337524F, -0.0F, -0.8203047513961791F);
        this.Shape41 = new ModelRenderer(this, 41, 12);
        this.Shape41.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape41.addBox(-2.5F, -12.0F, -0.5F, 5, 2, 2, 0.0F);
        this.setRotateAngle(Shape41, 0.0F, -0.0F, -0.8203047513961791F);
        this.Shape15 = new ModelRenderer(this, 63, 4);
        this.Shape15.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape15.addBox(-2.5F, -14.0F, 0.5F, 5, 4, 1, 0.0F);
        this.setRotateAngle(Shape15, 0.0F, -0.0F, -2.8710665702819824F);
        this.Shape53 = new ModelRenderer(this, 39, 16);
        this.Shape53.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape53.addBox(-2.5F, -24.0F, -1.5F, 1, 4, 2, 0.0F);
        this.setRotateAngle(Shape53, -0.1047197580337524F, -0.0F, 2.460914134979248F);
        this.Shape11 = new ModelRenderer(this, 110, 0);
        this.Shape11.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape11.addBox(-2.5F, -12.0F, -0.5F, 5, 2, 2, 0.0F);
        this.setRotateAngle(Shape11, 0.0F, -0.0F, 2.0507619380950928F);
        this.Shape39 = new ModelRenderer(this, 62, 0);
        this.Shape39.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape39.addBox(-0.5F, -9.0F, 0.5F, 1, 1, 1, 0.0F);
        this.setRotateAngle(Shape39, 0.0F, -0.0F, -1.5707963705062866F);
        this.Shape28 = new ModelRenderer(this, 99, 8);
        this.Shape28.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape28.addBox(0.5F, -12.5F, -2.0F, 2, 3, 3, 0.0F);
        this.setRotateAngle(Shape28, 0.0F, -0.0F, -2.8710665702819824F);
        this.Shape49 = new ModelRenderer(this, 51, 15);
        this.Shape49.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape49.addBox(-2.0F, -26.0F, -1.5F, 4, 2, 4, 0.0F);
        this.setRotateAngle(Shape49, -0.1047197580337524F, -0.0F, 2.460914134979248F);
        this.Shape8 = new ModelRenderer(this, 72, 0);
        this.Shape8.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape8.addBox(-2.5F, -11.9F, -1.5F, 5, 2, 2, 0.0F);
        this.setRotateAngle(Shape8, 0.0F, -0.0F, 2.4609142453120048F);
        this.Shape58 = new ModelRenderer(this, 86, 17);
        this.Shape58.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape58.addBox(0.5F, -21.0F, -1.5F, 1, 3, 2, 0.0F);
        this.setRotateAngle(Shape58, -0.1047197580337524F, -0.0F, -2.460914134979248F);
        this.Shape76 = new ModelRenderer(this, 19, 23);
        this.Shape76.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape76.addBox(-1.5F, -21.0F, -1.5F, 1, 3, 2, 0.0F);
        this.setRotateAngle(Shape76, -0.1047197580337524F, -0.0F, 0.8203047513961791F);
        this.Shape60 = new ModelRenderer(this, 114, 17);
        this.Shape60.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape60.addBox(-2.5F, -24.0F, -1.5F, 1, 4, 2, 0.0F);
        this.setRotateAngle(Shape60, -0.1047197580337524F, -0.0F, -2.460914134979248F);
        this.Shape3 = new ModelRenderer(this, 22, 0);
        this.Shape3.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape3.addBox(-2.5F, -12.0F, -0.5F, 5, 2, 2, 0.0F);
        this.setRotateAngle(Shape3, 0.0F, -0.0F, -2.0507619380950928F);
        this.Shape22 = new ModelRenderer(this, 34, 0);
        this.Shape22.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape22.addBox(-0.5F, 8.0F, 0.5F, 1, 1, 1, 0.0F);
        this.setRotateAngle(Shape22, 0.0F, -0.0F, -1.5707963705062866F);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) { 
        this.Shape13.render(f5);
        this.Shape5.render(f5);
        this.Shape51.render(f5);
        this.Shape26.render(f5);
        this.Shape83.render(f5);
        this.Shape4.render(f5);
        this.Shape20.render(f5);
        this.Shape77.render(f5);
        this.Shape36.render(f5);
        this.Shape42.render(f5);
        this.Shape63.render(f5);
        this.Shape14.render(f5);
        this.Shape55.render(f5);
        this.Shape10.render(f5);
        this.Shape66.render(f5);
        this.Shape69.render(f5);
        this.Shape30.render(f5);
        this.Shape32.render(f5);
        this.Shape40.render(f5);
        this.Shape56.render(f5);
        this.Shape44.render(f5);
        this.Shape73.render(f5);
        this.Shape21.render(f5);
        this.Shape31.render(f5);
        this.Shape62.render(f5);
        this.Shape37.render(f5);
        this.Shape27.render(f5);
        this.Shape57.render(f5);
        this.Shape9.render(f5);
        this.Shape72.render(f5);
        this.Shape74.render(f5);
        this.Shape7.render(f5);
        this.Shape61.render(f5);
        this.Shape70.render(f5);
        this.Shape80.render(f5);
        this.Shape24.render(f5);
        this.Shape64.render(f5);
        this.Shape25.render(f5);
        this.Shape23.render(f5);
        this.Shape12.render(f5);
        this.Shape54.render(f5);
        this.Shape48.render(f5);
        this.Shape43.render(f5);
        this.Shape35.render(f5);
        this.Shape59.render(f5);
        this.Shape46.render(f5);
        this.Shape47.render(f5);
        this.Shape67.render(f5);
        this.Shape1.render(f5);
        this.Shape16.render(f5);
        this.Shape50.render(f5);
        this.Shape78.render(f5);
        this.Shape17.render(f5);
        this.Shape33.render(f5);
        this.Shape45.render(f5);
        this.Shape19.render(f5);
        this.Shape68.render(f5);
        this.Shape34.render(f5);
        this.Shape75.render(f5);
        this.Shape71.render(f5);
        this.Shape79.render(f5);
        this.Shape38.render(f5);
        this.Shape29.render(f5);
        this.Shape52.render(f5);
        this.Shape18.render(f5);
        this.Shape81.render(f5);
        this.Shape2.render(f5);
        this.Shape65.render(f5);
        this.Shape82.render(f5);
        this.Shape41.render(f5);
        this.Shape15.render(f5);
        this.Shape53.render(f5);
        this.Shape11.render(f5);
        this.Shape39.render(f5);
        this.Shape28.render(f5);
        this.Shape49.render(f5);
        this.Shape8.render(f5);
        this.Shape58.render(f5);
        this.Shape76.render(f5);
        this.Shape60.render(f5);
        this.Shape3.render(f5);
        this.Shape22.render(f5);
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
	
}
