package lucraft.mods.heroes.speedsterheroes.util;

public class SHNBTTags {

	public static final String helmetOpen = "helmetOpen";
	public static final String hasSpeedforce = "hasSpeedforce";
	public static final String isInSpeed = "isInSpeed";
	public static final String velocityPoints = "velocityPoints";
	public static final String speedsterType = "speedsterType";
	public static final String velocity = "velocity";
	public static final String isSpeedforceTimerActive = "isSpeedforceTimerActive";
	public static final String speedforceTimer = "speedforceTimer";
	public static final String isBase = "isBase";
	public static final String isEnabled = "isEnabled";
	public static final String isConnected = "isConnected";
	public static final String blockPosX = "blockPosX";
	public static final String blockPosY = "blockPosY";
	public static final String blockPosZ = "blockPosZ";
	public static final String isSolid = "isSolid";
	public static final String facing = "facing";
	public static final String progress = "progress";
	public static final String waterBuckets = "waterBuckets";
	public static final String plateAmount = "plateAmount";
	public static final String hasTachyonPrototype = "hasTachyonPrototype";
	public static final String hasRing = "hasRing";
	public static final String traveledBlocks = "traveledBlocks";
	public static final String speedsterLevel = "speedsterLevel";
	public static final String speedsterPoints = "speedsterPoints";
	public static final String extraSpeedLevels = "extraSpeedLevels";
	public static final String v9Duration = "extra9DurationSpeedLevels";
	public static final String v9Timer = "v9Timer";
	public static final String phasingActive = "phasingActive";
	public static final String wraithTimer = "wraithTimer";
	public static final String type = "type";
	public static final String renderer = "renderer";
	public static final String red = "red";
	public static final String green = "green";
	public static final String blue = "blue";
	
}